const CLOCKWISE = 0;
const COUNTER_CLOCKWISE = 1;

function initGrid(grid = [], functionality = []) {
    grid = [
        [],
        []
    ];

    return () => {
        // is function of grid no doubt
        function spawnModule(place, moduleRef) {
            grid[place.x][place.y] = moduleRef;
        }

        function changeGrid(newGrid) {
            grid = newGrid;
        }

        return {
            grid,
            spawnModule
        };
    };
}

function initMatrix(grid) {
    if(!grid) grid = initGrid(); // r to fn

    return {
        grid
    };
}

function module(core, ...components) {
    let grid = [[components],[],[],[]];
}

function core(type, speed) {
    switch(type) {
        case 0: {
            return +1;
        }
        case 1: {
            return -1;
        }
        default: {
            return +1;
        }
    }
}

function component(identifier) {
    let exists = true;
    return identifier;
}

let cl = console.log;

let matrix = initMatrix();